/*
    *
    * This file is a part of CoreFM.
    * A file manager for C Suite.
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, write to the Free Software
    * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
    * MA 02110-1301, USA.
    *
*/


#include <QFileInfo>
#include <QDir>

#include <cprime/themefunc.h>
#include <cprime/filefunc.h>

#include "settings.h"

settings::settings()
{
	defaultSett = CPrime::Variables::CC_System_ConfigDir() + "coreapps/coreapps.conf";

    // set some default settings that are user specific
	if ( not QFileInfo::exists( defaultSett ) ) {
        cSetting = new QSettings("coreapps", "coreapps");
        qDebug() << "Settings file " << cSetting->fileName();

        CPrime::FileUtils::setupFolder(CPrime::FolderSetup::ConfigFolder);

        setDefaultSettings();
    } else {
        cSetting = new QSettings(defaultSett, QSettings::NativeFormat);
	}
}

settings::~settings()
{
	delete cSetting;
}

void settings::setDefaultSettings()
{
    cSetting->setValue("CoreApps/KeepActivities", true);
    cSetting->setValue("CoreApps/UseSystemNotification", true);
    cSetting->setValue("CoreApps/EnableExperimental", false);
    cSetting->setValue("CoreApps/AutoDetect", true);

    if (autoUIMode() == 2) {
        cSetting->setValue("CoreApps/IconViewIconSize", QSize(56, 56));
        cSetting->setValue("CoreApps/ListViewIconSize", QSize(48, 48));
        cSetting->setValue("CoreApps/ToolsIconSize", QSize(48, 48));
    } else {
        cSetting->setValue("CoreApps/IconViewIconSize", QSize(48, 48));
        cSetting->setValue("CoreApps/ListViewIconSize", QSize(32, 32));
        cSetting->setValue("CoreApps/ToolsIconSize", QSize(24, 24));
    }

    // App specific settings
    cSetting->setValue("CoreFM/ViewMode", 1);
    cSetting->setValue("CoreFM/ShowHidden", false);
    cSetting->setValue("CoreFM/ShowThumb", true);
}

int settings::autoUIMode() const
{
    int formFactor = CPrime::ThemeFunc::getFormFactor();
    int touchMode = CPrime::ThemeFunc::getTouchMode();

    if (formFactor == 2) {
        return 2; // Mobile
    } else if (formFactor == 1 && touchMode == 1) {
        return 1; // Tablet
    } else {
        return 0; // Desktop
    }
}

settings::cProxy settings::getValue(const QString &appName, const QString &key,
                                    const QVariant &defaultValue)
{
    if (appName == "CoreApps" && key == "UIMode") { // Wants to get CoreApps/UIMode
        // Check whether CoreApps/AutoDetect is On
        bool isAutoDetect = cSetting->value("CoreApps/AutoDetect").toBool();

        if (isAutoDetect)
            return cProxy { cSetting, "Dummy", autoUIMode() };
    }

    return cProxy{ cSetting, appName + "/" + key, defaultValue };
}

void settings::setValue(const QString &appName, const QString &key, QVariant value)
{
	cSetting->setValue(appName + "/" + key, value);
	cSetting->sync();
}

QString settings::defaultSettingsFilePath() const
{
    return cSetting->fileName();
}
