/*
	*
	* This file is a part of CoreFM.
	* A file manager for C Suite.
    * Copyright 2019 CuboCore Group
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* (at your option) any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/


#pragma once

#include <QAbstractItemModel>

#include "global.h"

class CTrashNode {

public:
	CTrashNode( const QString &path, const QString &trashPath, const QString &date );
    QVariant data( int, int );

private:
    QString mName;
    QString mPath;
    QDateTime mDate;
    QString mTrashPath;
    QIcon mIcon;
};

typedef QList<CTrashNode *> TrashNodes;

class CTrashModel : public QAbstractItemModel {

    Q_OBJECT

public:
    CTrashModel();
    ~CTrashModel();

    int rowCount( const QModelIndex &parent = QModelIndex() ) const;
    int columnCount( const QModelIndex &parent = QModelIndex() ) const;

    QVariant data( const QModelIndex &index, int role = Qt::DisplayRole ) const;
    QVariant headerData( int column, Qt::Orientation, int role ) const;

    QModelIndex index( int row, int column, const QModelIndex &parent ) const;
    QModelIndex parent( const QModelIndex & ) const;

    Qt::ItemFlags flags( const QModelIndex &index ) const;

    void reload();

private:
    void setupModelData();

    TrashNodes nodes;

};
