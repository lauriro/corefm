/*
	*
	* This file is a part of CoreFM.
	* A file manager for C Suite.
	* Copyright 2019 CuboCore Group
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* (at your option) any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/

#include "filedialog.h"
#include "ui_filedialog.h"

#include <QToolButton>
#include <QShortcut>

#include <cprime/messageengine.h>

filedialog::filedialog(filedialog::Mode mode, QDir curDir, QString text,  QWidget *parent)
	: QDialog(parent)
	, ui(new Ui::filedialog)
{
    ui->setupUi(this);

	setWindowFlags(Qt::Dialog);
	setAttribute(Qt::WA_NativeWindow);

    ui->done->setEnabled(0);
	connect(ui->cancel, &QToolButton::clicked, this, &filedialog::reject);
    connect(ui->done, &QToolButton::clicked, this, &filedialog::accept);

    shotcuts();

    dlgMode = mode;

    dir = QDir( curDir );
    dir.makeAbsolute();

    if ( !text.isNull() )
        data = QString( text );


    if ( mode == filedialog::NewFolder ) {
        setWindowTitle( "CoreFM - New Folder" );

        ui->lbl->setText( tr( "Create a new folder in <b>%1</b> named:" ).arg( curDir.path() ) );
        ui->lbl->setWordWrap( true );
        ui->newName->setText( "New Folder" );
        ui->newName->selectAll();
    }

    else if ( mode == filedialog::NewFile ) {
        setWindowTitle( "CoreFM - New File" );

        ui->lbl->setText( tr( "Create a new file in <b>%1</b> named:" ).arg( curDir.path() ) );
        ui->lbl->setWordWrap( true );
        ui->newName->setText( "New File" );
        ui->newName->selectAll();
    }

    else {
        setWindowTitle( "CoreFM - Rename" );

        QMimeType mt = mimeDbInstance.mimeTypeForFile( curDir.filePath( text ) );
        QFileInfo info( curDir.filePath( text ) );

        ui->done->setText( "Rename" );

        ui->lbl->setText( tr( "Rename <b>%1</b> in <b>%2</b> folder to:" ).arg( info.completeBaseName() ).arg( curDir.path() ) );
        ui->lbl->setWordWrap( true );
        ui->newName->setText( info.completeBaseName() + ( info.isDir() ? "" : "." + info.suffix() ) );
        ui->newName->setSelection( 0, info.completeBaseName().size() );
    }


    connect( ui->newName, SIGNAL( textEdited( QString ) ), this, SLOT( handleTextChanged( QString ) ) );
    connect( ui->newName, SIGNAL( returnPressed() ), this, SLOT( accept() ) );

    ui->newName->setFocus();

    handleTextChanged( ui->newName->text() );

    resize(400,120);
}

filedialog::~filedialog()
{
    delete ui;
}

void filedialog::shotcuts()
{
    QShortcut* shortcut;

    shortcut = new QShortcut(QKeySequence(Qt::Key_Enter), this);
    connect(shortcut, &QShortcut::activated, this, &filedialog::accept);
}

QString filedialog::getCurrentText() const
{
    return ui->newName->text();
}

QString filedialog::name() {

    return ui->newName->text();
}

void filedialog::cancel() {

    ui->newName->clear();
    reject();
};

void filedialog::handleTextChanged( QString newText ) {

    if ( newText.isEmpty() ) {
        ui->done->setDisabled( true );
        return;
    }

    if ( dir.entryList().contains( newText ) )
        ui->done->setDisabled( true );

    else
        ui->done->setEnabled( true );
};

void filedialog::accept() {

    switch( dlgMode ) {
        case filedialog::NewFolder: {
            if ( not dir.mkdir( ui->newName->text() ) )
                CPrime::MessageEngine::messageEngine("CoreFM", "corefm", "Can't create new folder", "Unable to create new folder at <b>" + dir.path() + "</b>");

            break;
        }

        case filedialog::NewFile: {
            QFile file( dir.filePath( ui->newName->text() ) );
            if ( not file.open( QFile::WriteOnly ) )
                CPrime::MessageEngine::messageEngine("CoreFM", "corefm", "Can't create new file", "Unable to create new file at <b>" + dir.path() + "</b>" );

            file.close();

            break;
        }

        case filedialog::Rename: {
            QString source = dir.filePath( data );
            QString target = dir.filePath( ui->newName->text() );
            if ( not QFile::rename( source, target ) )
                CPrime::MessageEngine::messageEngine("CoreFM", "corefm", "Can't rename", "Unable to rename <b>" + data + "</b>" );

            break;
        }
    }

    QDialog::accept();
};
