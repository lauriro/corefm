/*
	*
	* This file is a part of CoreFM.
	* A file manager for C Suite.
    * Copyright 2019 CuboCore Group
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* (at your option) any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/


#pragma once

#include "filesystemmodel.h"
#include "filesystemview.hpp"
#include "propertiesw.h"
#include "dirtree.h"
#include "global.h"

class QStackedWidget;
class QLabel;

class FolderView : public QWidget {

    Q_OBJECT

public:
	FolderView(int viewMode, QWidget *parent = nullptr );

    /* Check if this widget has focus */
    bool hasFocus();

    /* Return the selected paths */
    QStringList selection();

    /* Clear the selected items */
    void clearSelection();

    /* Clear the selected items */
    void selectAll();

    /* Root path of this view */
    QString rootPath();

    /* Change Icon Size */
	void setIconSize(QSize iconView , QSize listView);

    /* Sorting */
    void sort( int column, Qt::SortOrder order = Qt::AscendingOrder );

    /* Update the view modes */
    void refreshView( int );

    bool canGoBack();
    bool canGoForward();
    bool canGoUp();

private:
    void setupUI();
    void makeConnections();

    FileSystemView *view;
    DetailsSystemView *view2;
    FileSystemModel *fsm;
    QStackedWidget *viewStack;
	QLabel *freeSpaceLbl, *selectionLbl;

    /* History */
    QStringList oldRoots;
    long curIndex = 0;

    /* Move across the windows */
    static bool moveItems;

public Q_SLOTS:
    void loadAddress( QString addr = QDir::homePath() );
    void changeDir( const QModelIndex& );
    void changeDir( QString );
    void refresh();
    void goBack();
    void goForward();
    void goUp();

private Q_SLOTS:
    void emitUpdateAddress() {

		emit updateAddress( fsm->rootPath() );
	};

protected:
	void paintEvent( QPaintEvent * ) override;

Q_SIGNALS:
    // void sizeCount( quint64, quint64, quint64 );
    void contextMenu( const QPoint & );
    void updateVarious();

    void openFile( QString );

    void updateAddress( const QString &addr );
};
